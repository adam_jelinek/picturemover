import os
import exifread
import time
import shutil
import hashlib
import sys
from pylint.checkers.utils import is_empty

def md5_for_file(in_file):
    md5 = hashlib.md5()
    while True:
        data = in_file.read()
        if not data:
            break
        md5.update(data)
    return md5.digest() 


class MediaMover(object):
    
    def __init__(self, base_src, base_target):
        self.base_src = base_src
        self.base_target = base_target
        self.stats = Stats()
        self.source_info = None
        self.target_info = None

    def process_pictures(self):
        for path, dirs, files in os.walk(self.base_src):  # @UnusedVariable
            self.process_files(path, files)

        return self.stats

    def image_details(self, file_path):
        img = open(file_path, 'rb')
        tags = exifread.process_file(img)

        if 'EXIF DateTimeOriginal' in tags:
            orig_date = str(tags['EXIF DateTimeOriginal'])
            try:
                img_date = time.strptime(orig_date, '%Y:%m:%d %H:%M:%S')
            except:
                try:
                    img_date = time.strptime(orig_date, '%m.%d.%Y %H:%M:%S')
                except:
                    print 'Error processing date for file: ' + file_path    
                    raise
            
            return img_date

    def process_files(self, path, files):
        for file_name in files:
            self.skip = False
            self.source_info = PathDetails.create_by_path_and_name(path, file_name)
            
            ext = self.source_info.extension.upper()
            source_full_path = self.source_info.full_path

            if ext == '.JPG':
                self.stats.proccessed_image_count += 1
                self.target_info = self.build_img_path()

                if self.skip == False:
                    self.copy_file(source_full_path, self.target_info.full_path)

            elif ext in ('.INI', '.DB', '.INFO'):
                self.stats.system_files += 1

            elif ext in ('.THM', '.AVI', '.MOV', '.3GP', '.MP4', '.MPG'):
                self.stats.movie_count += 1
                self.copy_file(source_full_path, self.base_target + '/movies/' + file_name)

            else:
                self.stats.other_count += 1

    def build_img_path(self):
        """Build the target paths for images based on image details. This will include the path and name"""

        details = self.image_details(self.source_info.full_path)
        
        if details is None or is_empty(details):
            self.stats.no_meta_data += 1
            return PathDetails.create_by_qualified_path(os.path.join(self.base_target,  'nodetails', self.source_info.file_name))

        def convert_and_padd(num):
            return str(num).zfill(2)

        year = str(details.tm_year)
        month = convert_and_padd(details.tm_mon)
        
        file_name = month + '-' + convert_and_padd(details.tm_mday) + '-' + year + ' '
        file_name = file_name + convert_and_padd(details.tm_hour) + '.'
        file_name = file_name + convert_and_padd(details.tm_min) + '.'
        file_name = file_name + convert_and_padd(details.tm_sec)
        file_name = file_name + self.source_info.extension
        
        path = os.path.join(self.base_target, year, month, file_name)
        
        return PathDetails.create_by_qualified_path(self.check_unique(path))
    
    def check_unique(self, path):
        unique_tic = 0
        wrk_path = path
        dup_name = False

        def dup_hash():
            source_hash = md5_for_file(open(self.source_info.full_path, 'rb')) 
            target_hash = md5_for_file(open(wrk_path, 'rb'))
            return source_hash == target_hash
            
        while os.path.isfile(wrk_path):
            if dup_hash():
                self.stats.exact_dup += 1
                self.skip = True
                return path
            else:
                dup_name = True
                unique_tic += 1
                wrk_target = PathDetails.create_by_qualified_path(path)
                wrk_path = os.path.join(wrk_target.base_path, wrk_target.base_file_name + '_' + str(unique_tic) + wrk_target.extension)
                
        if dup_name: self.stats.dup_name +=1
        
        return wrk_path


    def copy_file(self, source_path, target_path):

        def check_dir(dir_path):
            if not os.path.isdir(dir_path):
                os.makedirs(dir_path)

        check_dir(os.path.split(target_path)[0])

        print 'Processed Images: ' + str(self.stats.proccessed_image_count) + '  Moving: ' + source_path 
        shutil.move(source_path, target_path)
        self.stats.moved_files += 1
        #shutil.copy2(source, l_target)
   
    
class Stats(object):
    def __init__(self):
        self.proccessed_image_count = 0
        self.moved_files = 0
        self.system_files = 0
        self.movie_count = 0
        self.other_count = 0
        self.exact_dup = 0
        self.dup_name = 0
        self.no_meta_data = 0

class PathDetails(object):
    def __init__(self):
        self.full_path = ''
        self.base_path = ''
        self.file_name = ''
        self.base_file_name = ''
        self.extension = ''
        
    def __parse_details__(self, full_path):
        self.full_path = full_path

        temp = os.path.split(self.full_path)
        self.base_path = temp[0] 
        self.file_name = temp[1]

        temp = os.path.splitext(self.file_name)
        self.base_file_name = temp[0]
        self.extension = temp[1]
    
    @classmethod
    def create_by_qualified_path(cls, full_path):
        details = PathDetails()
        details.__parse_details__(full_path);
        return details
        
    @classmethod
    def create_by_path_and_name(cls, path, file_name):
        details = PathDetails()
        details.__parse_details__(os.path.join(path, file_name));
        return details
        
        

if __name__ == '__main__':
    source_dir = sys.argv[1]
    target_dir = sys.argv[2]
    if os.path.isdir(source_dir):
        print MediaMover(source_dir, target_dir).process_pictures().__dict__
    else:
        print 'Not a Valid Source or Dir'
