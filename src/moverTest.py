import unittest
import os
import shutil
from mover import MediaMover, PathDetails

class TestMover(unittest.TestCase):

    src_loc = '../tst/src_test'
    clean_loc = '../tst/src_clean'
    target_loc = '../tst/target'
    test_image = '../tst/src_test/Kaitlyn.JPG'
    test_image_dup = '../tst/src_test/Kaitlyn2.JPG'
    test_image_mark = '../tst/src_test/KaitlynMark.JPG'
    test_image_mark2 = '../tst/src_test/KaitlynMark2.JPG'
    test_image_no_meta_data = '../tst/src_test/NoMetaData.jpg'
    
    def setUp(self):
        self.mover = MediaMover(self.src_loc, self.target_loc)
        self.move_setup()
        
    def test_PathDetails_parse(self):
        def assert_results(path_details):
            self.assertEqual(path_details.full_path, self.test_image)
            self.assertEqual(path_details.base_path, '../tst/src_test')
            self.assertEqual(path_details.file_name, 'Kaitlyn.JPG')
            self.assertEqual(path_details.base_file_name, 'Kaitlyn')
            self.assertEqual(path_details.extension, '.JPG')
        
        assert_results(PathDetails.create_by_qualified_path(self.test_image))
        assert_results(PathDetails.create_by_path_and_name(self.src_loc, 'Kaitlyn.JPG'))

    def test_file_counts(self):
        stats = self.mover.process_pictures()
        self.assertEquals(8, stats.proccessed_image_count)
        self.assertEquals(2, stats.dup_name)
        self.assertEquals(1, stats.exact_dup)
        self.assertEquals(2, stats.no_meta_data)
        self.assertEquals(6, stats.movie_count)
        self.assertEquals(2, stats.system_files)
        self.assertEquals(1, stats.other_count)

    def test_file_details(self):
        img_date = self.mover.image_details(self.test_image);
        self.assertEquals(2014, img_date.tm_year); 
        self.assertEquals(5, img_date.tm_mon); 
        self.assertEquals(18, img_date.tm_mday); 
        self.assertEquals(11, img_date.tm_hour); 
        self.assertEquals(7, img_date.tm_min); 
        self.assertEquals(1, img_date.tm_sec); 

    def test_image_path(self):
        self.mover.source_info = PathDetails.create_by_qualified_path(self.test_image)
        path = self.mover.build_img_path()
        self.assertEquals('../tst/target/2014/05/05-18-2014 11.07.01.JPG', path.full_path)
    
    def test_image_path_no_details(self):
        self.mover.source_info = PathDetails.create_by_qualified_path(self.test_image_no_meta_data)
        path = self.mover.build_img_path()
        self.assertEquals(path.full_path, '../tst/target/nodetails/NoMetaData.jpg')
        
    def test_duplicate_hash_found(self):
        target = self.target_loc + '/2099/10/image1.jpg'
        self.mover.copy_file(self.test_image, target)
        self.mover.source_info = PathDetails.create_by_qualified_path(self.test_image_dup)

        self.mover.check_unique(target);
        self.assertTrue(self.mover.skip, 'Image should be skipped')
    
    def test_same_file_name_incremented(self):
        #setup image in target 
        target = self.target_loc + '/2099/10/image.jpg'
        self.mover.copy_file(self.test_image, target)

        
        #First try should end in a _1
        #Move a similar image that will have a diff MD5
        self.mover.source_info = PathDetails.create_by_qualified_path(self.test_image_mark)
        path = self.mover.check_unique(target) #Target should be found and new path returned
        self.assertEqual(path, '../tst/target/2099/10/image_1.jpg')
        
        #setup second test
        target_2 = path
        self.mover.copy_file(self.test_image_mark, target_2)
        
        #Send dup should end in a _2
        self.mover.source_info = PathDetails.create_by_qualified_path(self.test_image_mark2)
        path = self.mover.check_unique(target)
        self.assertEqual(path, '../tst/target/2099/10/image_2.jpg')

    def test_copy_file(self):
        target = self.target_loc + '/2099/10/image1.jpg'
        self.mover.copy_file(self.test_image, target)
        self.assertFalse(os.path.isfile(self.test_image), 'Image should be removed from source')
        self.assertTrue(os.path.isfile(target), 'File should be in now location')

    def move_setup(self):
        if os.path.isdir(self.target_loc):
            shutil.rmtree(self.target_loc)

        if os.path.isdir(self.src_loc):
            shutil.rmtree(self.src_loc)

        shutil.copytree(self.clean_loc, self.src_loc)

if __name__ == '__main__':
    unittest.main()
